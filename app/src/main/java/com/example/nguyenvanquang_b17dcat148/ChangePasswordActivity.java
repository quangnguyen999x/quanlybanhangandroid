package com.example.nguyenvanquang_b17dcat148;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.nguyenvanquang_b17dcat148.api.ApiService;
import com.example.nguyenvanquang_b17dcat148.data.UpdatePassword;
import com.example.nguyenvanquang_b17dcat148.data_local.DataLocalManager;
import com.example.nguyenvanquang_b17dcat148.databinding.ActivityChangePasswordBinding;
import com.example.nguyenvanquang_b17dcat148.models.User;
import com.example.nguyenvanquang_b17dcat148.util.CheckStatusCode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    private ActivityChangePasswordBinding binding;
    private User user;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_change_password);
        binding = ActivityChangePasswordBinding.inflate(getLayoutInflater());

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait ...");

        View v = binding.getRoot();
        setContentView(v);
        getUser();

        binding.btSubmitPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.oldPass != null && binding.newPass != null && binding.comfirmPass != null) {
                    System.out.println("Không vào 1");
                    if (binding.newPass.getText().toString().equals(binding.comfirmPass.getText().toString())) {
                        System.out.println("Không vào 2");
                        UpdatePassword updatePassword = new UpdatePassword(binding.oldPass.getText().toString(), binding.newPass.getText().toString(),binding.comfirmPass.getText().toString());
                        changePass(updatePassword);
                    } else {
                        return;
                    }
                }

                return;
            }
        });
    }

    private void changePass(UpdatePassword updatePassword) {
        mProgressDialog.show();

        ApiService.apiService.changePassword(updatePassword).enqueue(new Callback<UpdatePassword>() {
            @Override
            public void onResponse(Call<UpdatePassword> call, Response<UpdatePassword> response) {
                mProgressDialog.dismiss();
                response = CheckStatusCode.checkToken(response, ChangePasswordActivity.this);

                if (response.body() != null) {

                    Toast.makeText(ChangePasswordActivity.this, "Success Call", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UpdatePassword> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(ChangePasswordActivity.this, "Error Call", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUser() {
        Integer id = DataLocalManager.getLoginResponse().getId();

        ApiService.apiService.getUserById(id).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                response = CheckStatusCode.checkToken(response, ChangePasswordActivity.this);

                if (response.body() != null) {
                    user = response.body();
                    setImageUser(user.getPhotosImagePath(), binding.imageviewAccountProfile);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(ChangePasswordActivity.this, "Error Call", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setImageUser(String url, ImageView imgView) {
        Glide.with(ChangePasswordActivity.this)
                .load(url)
                .placeholder(R.drawable.default_user)
                .circleCrop()
                .into(imgView);
    }
}