package com.example.nguyenvanquang_b17dcat148.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.example.nguyenvanquang_b17dcat148.EditUserActivity;
import com.example.nguyenvanquang_b17dcat148.R;
import com.example.nguyenvanquang_b17dcat148.inteface.IProductManagerClick;
import com.example.nguyenvanquang_b17dcat148.models.Product;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManagerProductAdapter extends RecyclerView.Adapter<ManagerProductAdapter.ManagerViewHolder>{
    private List<Product> mlist;
    private ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private Context mContext;

    private IProductManagerClick iProductManagerClick;

    public void callBack(IProductManagerClick iProductManagerClick) {
        this.iProductManagerClick = iProductManagerClick;
    }

    public ManagerProductAdapter(Context mContext) {
        mlist = new ArrayList<>();
        this.mContext = mContext;
    }

    public void setData(List<Product> mlist) {
        this.mlist = mlist;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ManagerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_manager_product, parent, false);
        return new ManagerProductAdapter.ManagerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ManagerProductAdapter.ManagerViewHolder holder, int position) {
        Product product = mlist.get(position);
        if (product == null) {
            return;
        }
        viewBinderHelper.bind(holder.swipeRevealLayout, String.valueOf(product.getId()));

        setImageProduct(product.getMainImagePath(),holder.circleImageViewProduct ,mContext);
        holder.tvName.setText("Name: " + product.getName());
        holder.tvAlias.setText("Alais: " + product.getAlias());
        holder.tvPrice.setText("Price: " + product.getPrice() +"$");
        holder.tvDiscount.setText("Discount: " +product.getDiscountPercent()+"$");

        holder.layoutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, EditUserActivity.class); // Chỉnh sửa
                intent.putExtra("product", product);
                mContext.startActivity(intent);
            }
        });

        holder.layoutDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mlist.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
                iProductManagerClick.onClickRemoveProduct(product); // Chỉnh sửa
            }
        });
    }

    private void setImageProduct(String img, ImageView imgView, Context mcontext) {
        Glide.with(mcontext)
                .load(img)
                .placeholder(R.drawable.default_user)
                .into(imgView);
    }

    @Override
    public int getItemCount() {
        if (mlist != null) {
            return mlist.size();
        }
        return 0;
    }

    public class ManagerViewHolder extends RecyclerView.ViewHolder {
        private SwipeRevealLayout swipeRevealLayout;
        private TextView layoutDelete;
        private TextView layoutEdit;
        private ImageView circleImageViewProduct;
        private TextView tvName, tvAlias, tvPrice, tvDiscount;

        public ManagerViewHolder(@NonNull View itemView) {
            super(itemView);

            swipeRevealLayout = itemView.findViewById(R.id.swipeReveal_manager);
            layoutDelete = itemView.findViewById(R.id.layout_delete_manager);
            layoutEdit = itemView.findViewById(R.id.layout_edit_manager);
            circleImageViewProduct = itemView.findViewById(R.id.manager_image);
            tvName = itemView.findViewById(R.id.txt_manager_name);
            tvAlias = itemView.findViewById(R.id.txt_manager_alias);
            tvPrice = itemView.findViewById(R.id.txt_manager_price);
            tvDiscount = itemView.findViewById(R.id.txt_manager_discount);
        }
    }
}
