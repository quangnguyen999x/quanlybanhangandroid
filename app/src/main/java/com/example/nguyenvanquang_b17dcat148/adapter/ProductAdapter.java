package com.example.nguyenvanquang_b17dcat148.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.nguyenvanquang_b17dcat148.ProductDetailActivity;
import com.example.nguyenvanquang_b17dcat148.R;
import com.example.nguyenvanquang_b17dcat148.models.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int TYPE_ITEM=1;
    private static final int TYPE_LOADING=2;

    private boolean isLoadingAdd;

    private List<Product> mlistProduct;
    private Activity activity;

    public ProductAdapter(Activity activity){
        mlistProduct = new ArrayList<>();
        this.activity = activity;
    }

    public void setData(List<Product> mlist) {
        this.mlistProduct = mlist;
        notifyDataSetChanged(); // should be replaced with DiffUtil lib to notifyDataChange
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);

            return new ProductViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == TYPE_ITEM) {
            Product product = mlistProduct.get(position);

            if (product == null) {
                return;
            }

            ProductViewHolder productViewHolder = (ProductViewHolder) holder;

            setImageProduct(product.getMainImagePath(), productViewHolder.imgProduct, holder.itemView.getContext());
//        holder.imgProduct.setImageResource(product.getImgResouce());
            productViewHolder.tvProductName.setText(product.getName());
//        holder.tvDescription.setText(product.getShortDescription());
            productViewHolder.tvPrice.setText("$" +product.getPrice());

            holder.itemView.setOnClickListener(new View.OnClickListener() { // Go to productDetail
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ProductDetailActivity.class);
                    intent.putExtra("product", product);
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mlistProduct != null && mlistProduct.size() - 1 == position && isLoadingAdd == true) { // Khác null và là vị trí cuối

            return TYPE_LOADING;
        }

        return TYPE_ITEM;
    }

    private void setImageProduct(String img, ImageView imgView, Context mcontext) {
        Glide.with(mcontext)
                .load(img)
                .placeholder(R.drawable.default_user)
                .into(imgView);
    }

    @Override
    public int getItemCount() {
        if (mlistProduct != null) {
            return mlistProduct.size();
        }
        return 0;
    }

    public void addFooterLoading() { // add element null to end of list
        isLoadingAdd = true;
        mlistProduct.add(new Product());// nhét tk Null vào cuối, đợi data load lên r xóa
    }

    public void removeFooterLoading() {
        isLoadingAdd = false;
        int postion = mlistProduct.size() - 1;
        // remove element end add when call loading
        Product proIndex = mlistProduct.get(postion);

        if (proIndex != null) {
            mlistProduct.remove(proIndex);
            notifyDataSetChanged();
        }
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        // Create view in item_product.xml
        private ImageView imgProduct;
        private TextView tvProductName;
//        private TextView tvDescription;
        private TextView tvPrice;
        private ImageView imgAddToCart;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProduct = itemView.findViewById(R.id.product_image);
//            imgAddToCart = itemView.findViewById(R.id.img_add_to_cart);
            tvProductName = itemView.findViewById(R.id.tv_product_name);
//            tvDescription = itemView.findViewById(R.id.tv_description);
            tvPrice = itemView.findViewById(R.id.tv_price);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
