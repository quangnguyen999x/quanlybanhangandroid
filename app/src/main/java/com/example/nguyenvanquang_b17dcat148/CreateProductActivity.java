package com.example.nguyenvanquang_b17dcat148;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.nguyenvanquang_b17dcat148.api.ApiService;
import com.example.nguyenvanquang_b17dcat148.databinding.ActivityCreateProductBinding;
import com.example.nguyenvanquang_b17dcat148.models.Product;
import com.example.nguyenvanquang_b17dcat148.models.User;
import com.example.nguyenvanquang_b17dcat148.util.CheckStatusCode;
import com.example.nguyenvanquang_b17dcat148.util.RealPathUtil;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedbottompicker.TedBottomSheetDialogFragment;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateProductActivity extends AppCompatActivity {

    private ActivityCreateProductBinding binding;
    private Uri mainUri;
    private List<Uri> extraUri = new ArrayList<>();
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_create_product);

        binding = ActivityCreateProductBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait ...");

        handlingFAB();

        binding.imgBackAddManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        binding.cancelManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        binding.createManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = new Product();

                String name = binding.addManagerName.getText().toString();
                String alais = binding.addManagerAlias.getText().toString();
                String fullDes = binding.addManagerFull.getText().toString();
                String shortDes= binding.addManagerShort.getText().toString();
                float price = Float.parseFloat(binding.addManagerPrice.getText().toString());
                float discount = Float.parseFloat(binding.addManagerDiscount.getText().toString());

                product.setName(name);
                product.setAlias(alais);
                product.setFullDescription(fullDes);
                product.setShortDescription(shortDes);
                product.setPrice(price);
                product.setDiscountPercent(discount);

                createProduct(product);
            }
        });

        binding.btnManagerExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissionMulti();
            }
        });
    }

    private void createProduct(Product product) {
        mProgressDialog.show();

        Gson gson = new Gson();
        String productJson = gson.toJson(product);
        RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), productJson);

        MultipartBody.Part multipartMain = null;
        if (mainUri != null) {
            String strRealPath = RealPathUtil.getRealPath(this, mainUri);
            Log.d("Tên đường dẫn file 1: ", strRealPath);
            File file = new File(strRealPath); // get Path of file in gallary

            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file); // Create Object multipart
            multipartMain = MultipartBody.Part.createFormData("fileImage", file.getName(), requestBody);
        } else {
            RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
            multipartMain = MultipartBody.Part.createFormData("fileImage", "", attachmentEmpty);
        }

        List<MultipartBody.Part> parts = new ArrayList<>();

        for (int i=0; i < extraUri.size(); i++){
            parts.add(prepareFilePart("extraImage", extraUri.get(i)));
        }

        ApiService.apiService.createProduct(body, multipartMain, parts).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                response = CheckStatusCode.checkToken(response, CreateProductActivity.this);
                mProgressDialog.dismiss();
                if (response.body() != null) {
                    Product productRes = response.body();
                    System.out.println("Xong " + productRes.getId());
                    Toast.makeText(CreateProductActivity.this, "Success call", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                mProgressDialog.dismiss(); // whether Fail or Success need turn off Progress
                Toast.makeText(CreateProductActivity.this, "Error call", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri){
        String strRealPath = RealPathUtil.getRealPath(this, fileUri);
        File file = new File(strRealPath);

        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);

        return MultipartBody.Part.createFormData(partName, file.getName(),requestBody);
    }

    private void handlingFAB() {
        binding.floatingActionButtonAddManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissionOne();
            }
        });
    }

    private void requestPermissionMulti() {
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openMultiImagePicker();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(CreateProductActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        TedPermission.with(this)
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    private void openMultiImagePicker() {
        TedBottomPicker.with(CreateProductActivity.this)
                .setPeekHeight(1600)
                .showTitle(false)
                .setCompleteButtonText("Done")
                .setEmptySelectionText("No Select")
//                .setSelectedUriList(selectedUriList)
                .showMultiImage(new TedBottomSheetDialogFragment.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(List<Uri> uriList) {
                        // here is selected image uri list
                        if (uriList != null && !uriList.isEmpty()) {
                            extraUri = uriList;
                        }
                    }
                });
    }

    private void requestPermissionOne() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) { // Chỉ từ android 6 trở lên: Just android 6 or higher
            return;
        }
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openImagePicker();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(CreateProductActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        TedPermission.with(this)
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    private void openImagePicker() {
        TedBottomPicker.with(CreateProductActivity.this)
                .show(new TedBottomSheetDialogFragment.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        // here is selected image uri
                        if (uri != null) {
                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(CreateProductActivity.this.getContentResolver(),uri);
                                mainUri = uri;
                                if (bitmap != null) {
                                    binding.imageviewManagerProfile.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }
}