package com.example.nguyenvanquang_b17dcat148.inteface;

import com.example.nguyenvanquang_b17dcat148.models.Product;

public interface IProductManagerClick {

    void onClickRemoveProduct(Product product);
}
