package com.example.nguyenvanquang_b17dcat148;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.nguyenvanquang_b17dcat148.adapter.ManagerProductAdapter;
import com.example.nguyenvanquang_b17dcat148.api.ApiService;
import com.example.nguyenvanquang_b17dcat148.databinding.ActivityManagerProductBinding;
import com.example.nguyenvanquang_b17dcat148.inteface.IProductManagerClick;
import com.example.nguyenvanquang_b17dcat148.models.Product;
import com.example.nguyenvanquang_b17dcat148.util.CheckStatusCode;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManagerProductActivity extends AppCompatActivity {

    private IProductManagerClick iProductManagerClick;
    private ActivityManagerProductBinding binding;
    private ManagerProductAdapter adapter;
    private List<Product> mlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_manager_product);

        binding = ActivityManagerProductBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.recyclerProduct.setLayoutManager(linearLayoutManager);

        adapter = new ManagerProductAdapter(this);
        binding.recyclerProduct.setAdapter(adapter);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        binding.recyclerProduct.addItemDecoration(itemDecoration);

        getAllProduct();

        iProductManagerClick = new IProductManagerClick() {
            @Override
            public void onClickRemoveProduct(Product product) {
                deleteProduct(product);
            }
        };

        adapter.callBack(iProductManagerClick);

        binding.floatingActionButtonProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerProductActivity.this, CreateProductActivity.class);
                startActivity(intent);
            }
        });

        binding.imgBackManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Chạy luồng này");
                getAllProduct();
            }
        }).start();
    }

    private void deleteProduct(Product product) {
        Integer id = product.getId();
        System.out.println("Product id nhé: " + id);
        ApiService.apiService.removeProduct(id).enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                response = CheckStatusCode.checkToken(response, ManagerProductActivity.this);
                if (response.body() != null) {
                    System.out.println("ID Product xóa: " + response.body());
                    Toast.makeText(ManagerProductActivity.this, "Delete success", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(ManagerProductActivity.this, "Error call", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAllProduct() {
        ApiService.apiService.listAllProduct().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                response = CheckStatusCode.checkToken(response, ManagerProductActivity.this);

                if (response.body() != null) {
                    mlist = response.body();
                    adapter.setData(mlist);

                    binding.recyclerProduct.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toast.makeText(ManagerProductActivity.this, "Error Call", Toast.LENGTH_SHORT).show();
            }
        });
    }
}