package com.example.nguyenvanquang_b17dcat148.util;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
// Same me: https://viblo.asia/p/tu-tao-event-load-more-phan-2-handle-load-more-load-failed-and-reach-end-WAyK874E5xX
public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager linearLayoutManager;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    public PaginationScrollListener(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    public PaginationScrollListener(StaggeredGridLayoutManager staggeredGridLayoutManager) {
        this.staggeredGridLayoutManager = staggeredGridLayoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = staggeredGridLayoutManager.getChildCount(); // Số bản ghi xuất hiện
        int totalItemCount = staggeredGridLayoutManager.getItemCount(); // Tổng số bản ghi 1 page
//        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();// Vị trí phần tử đầu tiên
//        int [] postion = new int[2];
        int[] firstVisibleItemPosition = staggeredGridLayoutManager.findFirstVisibleItemPositions(null);

        // Thuật toán phân trang
        if (isLoading() || isLastPage()) {// Đang tải dữ liệu hoặc là trang cuối cùng r. Không xử lý phân trang nữa
            return;
        }

        System.out.println("Hai vị trí : " + firstVisibleItemPosition[0] + " " + firstVisibleItemPosition[1]);

        if (firstVisibleItemPosition[1] >= 0 && (visibleItemCount + firstVisibleItemPosition[1]) >= totalItemCount) {
            loadMore();
        }
    }

    public abstract void loadMore();
    public abstract boolean isLoading();
    public abstract boolean isLastPage();
}
