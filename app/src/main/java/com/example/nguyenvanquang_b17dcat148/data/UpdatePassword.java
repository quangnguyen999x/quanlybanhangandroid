package com.example.nguyenvanquang_b17dcat148.data;

public class UpdatePassword {

    private String oldPassword;

    private String newPassword;

    private String confirmPass;

    public String getOldPassword() {
        return oldPassword;
    }

    public UpdatePassword(String oldPassword, String newPassword, String confirmPass) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmPass = confirmPass;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }
}
