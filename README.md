App quản lý shop bán hàng đồ điện tử Haku
Project tạo ra app cho phép người dùng có thể quản lý shop bán hàng đồ điện tử và
cho khác hàng tải app để có thể mua sản phẩm ở trên shop online.

**Chức năng chung:**

- Đăng nhập, đăng ký, quên mật khẩu, đổi mật khẩu, đăng xuất. Trạng thái
người dùng được check thông qua token server gửi lên và lưu và
SharedPreferences với cơ chế Singleton.

- Hiển thị danh sách sản phẩm cho người dùng thêm sản phẩm vào giỏ hàng,
xem thông tin giỏ hàng (thêm số lượng và xóa), xem sản phẩm chi tiết, xem
các hóa đơn đã thanh toán, cập nhật thông tin tài khoản người dùng (ảnh, họ
tên, ...), Search sản phẩm cần tìm, notification khi người dùng đã thanh toán
giỏ hàng thành công. Làm load more (paging) cho sản phẩm.

- Sử dụng camera quét mã Qrcode để thêm sản phẩm vào giỏ hàng

**Quản lý:**

- Quản lí danh sách người dùng, thêm, sửa, xóa (ảnh, thông tin, ...)

- Quản lí sản phẩm tìm kiếm thêm, sửa, xóa 1 sản phẩm (ảnh chính, các ảnh
phụ và thông tin sản phẩm)
